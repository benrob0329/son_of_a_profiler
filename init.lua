local profiler = dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/lua-profiler/src/profiler.lua")

profiler.configuration({
	fW = 80,
})

local function start()
	profiler.start()
end

local function stop()
	profiler.stop()
end

local function report()
	profiler.report(minetest.get_worldpath() .. "/profiler.log")
end

minetest.register_chatcommand("profile", {
	params = "<duration>",
	description = "Profile the server for <duration> seconds",
	privs = {server = true},
	func = function(duration)
    		start()
    		minetest.after(tonumber(duration) or 1, stop)
	end,
})

minetest.register_chatcommand("profile_report", {
	params = "",
	description = "Writes the profiler report to [world dir]/profiler.log",
	privs = {server = true},
	func = report,
})

if minetest.settings:get_bool("profile_at_startup") then
    start()
    minetest.register_on_shutdown(function()
        stop()
        report()
    end)
end

